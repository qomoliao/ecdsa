签名依赖非对称加密算法，非对称加密算法的理论依据是**单向陷门函数**。

以下要点整理来自知乎文章：[一文读懂ECDSA算法如何保护数据](https://zhuanlan.zhihu.com/p/97953640)
## 椭圆曲线
y^2 = (x^3 + ax + b)  mod p

曲线参数：a，b，p，N，G
a，b：是曲线参数
p：是模运算的底
N：曲线上面点个数（离散化）
G：参考起始点
> NIST(National Institute of Standards and Technology)和SECG(Standards for Efficient Cryptography Group)提供了预处理的标准化曲线参数。

## 加法与乘法运算
P + Q

![加法](./media/202104/加法运算_1618458350.jpg)

kP

![乘法](./media/202104/乘法运算_1618458371.jpg)

## 公私钥生成

私钥（dA）是一个随机数
公钥（Qa）是dA与起始点G的点乘
**Qa = dA × G**


## 签名
ECDS签名由值对**(R, S)**表示，下面看看如何生成这个值对。
1. 产生一个**随机数k**
2. 利用点乘法计算**P=k×G**
3. 点P的x坐标即为**R**
4. 利用SHA计算哈希**z**
5. 通过方程**S=k^-1(z + dA × R) mod p** 计算S

> k^-1是k的**模的乘法逆元**

## 校验算
计算点P：
**P = S^-1 × z × G + S^-1 × R × Qa**
如果P的x坐标与R相等，则签名有效，否则无效。




